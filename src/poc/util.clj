(ns poc.util
  (:gen-class)
  (:import (java.util Date))
  (:require  [clojure.tools.logging :as log]))

(defn dbg [x]
  (println x)
  x)

(defn lazy-contains? [col key]
  (some #{key} col))
