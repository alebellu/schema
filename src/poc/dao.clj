;; poc DAO
(ns poc.dao
  (:require [poc.schema :refer [schema dschema]]
            [poc.util :refer [dbg]]
            [clojure.tools.logging :as log]
            [datomic.api :as d]
            [clojure.java.io :as io]
            [clj-time.core :as t]
            [clj-time.coerce :as c]
            [clj-time.local :as l])
  (:import datomic.Util datomic.Datom
           (java.util Date)))

;; datomic datastore connection
(def uri "datomic:free://localhost:4334/poc2")

(def conn (d/connect uri))

(def min-time (Date. Long/MIN_VALUE))
(def max-time (Date. Long/MAX_VALUE))

;;; transaction time handling functions

(defn get-db
  "Returns a db object at transaction time tt
  tt is the Datomic transaction id (defaults to the latest)"
  [& [tt]]
  (if tt
    (d/as-of (d/db conn) tt)
    (d/db conn)))

;;; valid time handling functions

;(def ^:const from-id (let [db get-db] (d/entid db :validity/from)))
;(def ^:const to-id   (let [db get-db] (d/entid db :validity/to)))

(defn get-valid-db
  "Returns a db object valid at time vt and transaction time tt.
   vt can be a long (unix epoch in millis) or a java.util.Date object (defaults to now) tt is the Datomic transaction id (defaults to the latest)"
  [& [vtp tt]]
  (let [vt (c/to-long (if vtp vtp (Date.)))]
    (d/filter (get-db tt) (fn [db ^Datom datom]
      (let [track-validity (-> (d/entity db (.a datom)) :3e.validity/track)]
        ; (println " filtering..." (-> (d/touch (d/entity db (.a datom))) :db/ident) track-validity)
        (if track-validity
          (let [tx   (d/entity db (.tx datom))
                from (-> tx :3e.validity/from c/to-long)
                to   (-> tx :3e.validity/to c/to-long)]
         ; (println " filtering..." from to vt (>= vt from) tx)
            (and (>= vt from)
                 (or (not to) (< vt to))))
          true))))))

(defn get-attr-values
  "Returns all the values for an entity's attribute together with validity datetimes.
   Results are ordered by validity 'from' datetime"
  [db eid attr]
  (->> (d/q  '[:find ?from ?to ?v
               :in $ ?eid ?attr ?to
               :where
               [?eid ?attr ?v ?tx]
               [?tx :3e.validity/from ?from]
              ]
             db eid attr max-time)
       (sort-by first)))

(defn validate-tx
  "validates a transaction without executing it. Only the specified attributes are checked
  for validity. An exception is thrown if the entity is not valid in the context of the current database"
  [e eid attrs vtf vtt]
  (let [db (get-db)]
    ; for each attribute
    (doseq [attr attrs]
      ; if the attribute needs validity checks
      (when (-> dschema attr :3e.validity/track)
        ; if the attribute cardinality is one once the valid time is fixed
        (when (= (-> dschema attr :3e.validity/cardinality) :db.cardinality/one)
          ; find all entries and sort them by from date
          (let [existing-times (map #(take 2 %) (get-attr-values db eid attr))
                new-times      [vtf vtt]
                merged-times   (sort-by first (cons new-times existing-times))
                times          (map c/to-long (flatten merged-times))]
            ; then check that there is no overlapping
            (reduce (fn [t1 t2] (assert (<= t1 t2)) t2) times)))))))

(defn validate-tx-as-if
  "validates a transaction without executing it. Only the specified attributes are checked
  for validity. An exception is thrown if the entity is not valid in the context of the current database"
  [tx-data eid attrs]
  (let [db (d/with (get-db) tx-data)]
    (print tx-data)
    ; for each attribute
    (doseq [attr attrs]
      ; if the attribute needs validity checks
      (if (-> dschema attr :3e.validity/track)
        ; if the attribute cardinality is one once the valid time is fixed
        (if (= (-> dschema attr :3e.validity/cardinality) :db.cardinality/one)
          ; find all entries and sort them by from date
          (println  (get-attr-values db eid attr))
          (let [times-with-value  (get-attr-values db eid attr)
                times             (flatten (map times-with-value #(take 2 %)))]
            ; then check that there is no overlapping
            (println times-with-value times)
            (reduce #((assert (< %1 %2)) %2) times)))))))

;;; (C)RUD Create

(defn create-entity
  "creates a new entity
   a validity 'from' date (and optionally 'to' date) must be specified in case any of the included attributes
   have tracking of validity enabled"
  [type e & [vtf vtt]]
    (let [ee (assoc e :db/id (d/tempid :db.part/user)
                      :entity/uuid (d/squuid)
                      :entity/type type)
          vtt (if vtf (if vtt vtt max-time))
          tx-data (if vtf [ee
                             {:db/id (d/tempid :db.part/tx)
                              :3e.validity/from vtf
                              :3e.validity/to vtt}]
                          ee)]
      (validate-tx ee (ee :db/id) (keys ee) vtf vtt)
      (dbg (d/transact conn tx-data))
      ee))

;;; C(R)UD Read

(defn fetch-entities
  [db eids]
  (->> eids
       (map #(d/touch (d/entity db (first %))))
       seq))

(defn get-eid-by-uuid
  "retrieve the datomic id of an entity given the uuid"
  [db uuid]
    (ffirst
      (d/q '[:find ?p
             :in $ ?uuid
             :where
             [?p :entity/uuid ?uuid]]
         db (java.util.UUID/fromString uuid))))

(defn get-entity-by-uuid
  "retrieve an entity (optionally at a given valid time and/or transaction time)"
  [uuid & [vt tt]]
  (let [db  (get-valid-db vt tt)
        eid (get-eid-by-uuid db uuid)]
    (if eid (fetch-entities db #{[eid]}))))

(defn get-all
  "retrieve all entities at a given valid time (and optionally at a given transaction time)"
  [type & [vt tt]]
  (let [db   (get-valid-db vt tt)
        eids (d/q '[:find ?e
              :in $ ?type
              :where
              [?e :entity/type ?type]
              [?e :entity/uuid _]] ; check to ensure the entity existed at vt
              db, type)]
    (if (not (empty? eids)) (fetch-entities db eids) "")))

;;; CR(U)D Update

(defn update-entity
  "update an existing entity
   a validity from date (and optionally to date) must be specified in case any of the included attributes
   have tracking of validity enabled"
  [uuid e & [vtf vtt]]
    (let [db    (d/db conn)
          uuid (java.util.UUID/fromString uuid)
          vtt (if vtf (if vtt vtt max-time))
          ee (dissoc (assoc e
                      :entity/uuid uuid  ;; this finds the existing entity
                      :entity/type type
                      :db/id (d/tempid :db.part/user))  ;; will be replaced by exiting id
                :uuid)
          tx-data (if vtf [ee
                             {:db/id (d/tempid :db.part/tx)
                              :3e.validity/from vtf
                              :3e.validity/to vtt}]
                          ee)]
      (validate-tx ee (ee :db/id) (keys ee) vtf vtt)
      (dbg tx-data)
      (d/transact conn tx-data)
      ee))

;;; CRU(D) Delete

(defn delete-entity
  "delete an existing entity"
  [uuid]
    (d/transact conn [[:db.fn/retractEntity (get-eid-by-uuid (d/db conn) uuid)]])
    uuid)

;;; Transaction related functions

(defn get-all-tt
  []
   (->> (d/q '[:find ?tx ?tt
         :where [?tx :db/txInstant ?tt]]
         (get-db))
        vec
        (sort-by first)
        (map #(update-in % [0] d/tx->t))
        ))

(defn tt-to-datetime [tt]
    (ffirst (d/q '[:find ?tt
                   :in $ ?tx
                   :where [?tx :db/txInstant ?tt]]
                 (get-db)
                 (d/t->tx tt))))

(defn get-transaction-info
  "Return info about a transaction"
  [tt]
    {:tt tt
     :timestamp (tt-to-datetime tt)})

(defn last-time
  "Return the last used transaction info"
  []
  (get-transaction-info (d/basis-t (get-db))))

;;; Examples

(comment
  (get-all :client (t/plus (l/local-now) (t/months 1)) 10000)

  (get-all :client (t/minus (l/local-now) (t/months 1)))

  (time (let [db (get-valid-db)]
          (d/q '[:find ?e ?n
                 :where
                 [?e :client/name ?n]]
               db)))
)

