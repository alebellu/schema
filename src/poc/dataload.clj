;; poc test data loading
(ns poc.dataload
  (:require [poc.util :refer [dbg lazy-contains?]]
            [clojure.pprint :refer [pprint]]
            [clojure.tools.logging :as log]
            [clojure.java.io :as io]
            [clj-time.core :as t]
            [clj-time.coerce :as c]
            [clj-time.local :as l]
            [clojure-ini.core :refer [read-ini]]))

(defn park [park-name park-def]
  (println park-name (park-def :PARK_INFO))
  (println park-name :COMMON_WTG_INFO (park-def :COMMON_WTG_INFO))
  (let [turbines-def (apply dissoc park-def [:PARK_INFO :COMMON_WTG_INFO])]
    (doseq [[t-name t-def] turbines-def]
      (println park-name " turbine " t-name ": " t-def))))

(def park-dir (clojure.java.io/file "resources/data/poc2/TestData/park_config/"))
(def park-files (. park-dir (listFiles)))
(doseq [park-file park-files]
  (let [file-name (. park-file (getName))
        park-name (subs file-name 0 (- (count file-name) 4))]
    (park
      park-name
      (read-ini park-file :keywordize? true))))
