;; rest api poc
(ns poc.core
  (:gen-class)
  (:import (java.util Date))
  (:require  [poc.util :refer [dbg]]
             [clojure.tools.logging :as log]
             [clojure.pprint :refer [pprint]]
             [ring.util.response :refer [response status]]
             [ring.adapter.jetty :refer [run-jetty]]
             [ring.middleware.json :refer [wrap-json-response wrap-json-params wrap-json-body]]
             [compojure.core :refer :all]
             [compojure.route :as route]
             [compojure.handler :as handler]
             [poc.dao :as dao]))

(defn times-routes []
  (routes
    (GET "/times" [] {:body (dao/get-all-tt)})))

(defn keys-to-keywords [e]
  "Transform the input sequence to an hashmap where keys are transformed to keywords"
 (apply hash-map
   (flatten
     (for [[k v] e]
       [(keyword k) v]))))

(defn def-type-routes [type typedef]
  (let [gpath (str "/" (typedef :plural-name))
        spath (str gpath "/:uuid")]
    [(GET gpath {params :params}
          (let [vt (if (params :vt) (Long. (params :vt)))
                tt (if (params :tt) (Long. (params :tt)))]
            (dao/get-all type vt tt)))
     (GET spath {params :params}
          (let [uuid (params :uuid)
                tt   (if (params :tt)(Integer. (params :tt)))]
            {:body (dao/get-entity-by-uuid uuid tt)})) ; :body is needed, otherwise the return map is interpreted as options
     (POST gpath {params :params}
           (let [entity (keys-to-keywords params)]
             (dao/create-entity type entity (Date.) nil))) ; or:   (POST gpath request (dao/create-entity (request :body)))
     (PUT spath {params :params}
          (let [entity (keys-to-keywords params)]
            (dao/update-entity (entity :uuid) entity (Date.) nil)))
     (DELETE spath [uuid]
             (dao/delete-entity uuid))]))

(defn entities-routes []
  "entities routes handler"
  (apply routes (flatten (map (fn [[type typedef]] (def-type-routes type typedef)) dao/types))))

(defroutes app-routes
  "main handler"
  (GET "/" [] "<h1>SQ Rest API</h1>")
  (times-routes)
  (entities-routes)
  (route/not-found "<h1>Page not found</h1>"))

(defn with-error-catching [handler]
  (fn [request]
    (try
      (handler request)
      (catch Exception e
        (log/error e)
        (println e)
        [500 (str "Application Error: " e)]))))

(defn authorized?
  "check authorization"
  [request]
  true)

(defn wrap-auth [handler]
  (fn [request]
    (if (authorized? request)
      (handler request)
      (-> (response "Access-Control-Allow-Origin Denied")
          (status 403)))))

(defn allow-cors [handler]
  (fn [request]
    (let [response (handler request)]
      (update-in response
                 [:headers "Access-Control-Allow-Origin"]
                 (fn [_] "*")))))

;(def app
;  (allow-cors (wrap-auth (wrap-json-response (wrap-json-params (wrap-json-body (handler/site app-routes) {:keywords? true}))))))
(def app
  (-> app-routes
      with-error-catching
      handler/site
      wrap-json-body
      wrap-json-params
      wrap-json-response
      wrap-auth
      allow-cors))

(def server
  (run-jetty #'app {:port 8088 :join? false}))

(defn -main []
  (.start server))

(comment
  (.start server)
  (.stop server))
