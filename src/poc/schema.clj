;; poc DAO
(ns poc.schema
  (:require [poc.util :refer [dbg lazy-contains?]]
            [clojure.pprint :refer [pprint]]
            [clojure.tools.logging :as log]
            [datomic.api :as d]
            [clojure.java.io :as io]
            [clj-time.core :as t]
            [clj-time.coerce :as c]
            [clj-time.local :as l])
  (:import datomic.Util datomic.Datom
           (java.util Date)))

; This module defines 3 different schemas variables, available for use in external modules:
;
; schema:  high level domain schema (handwritted)
; eschema: effective schema, where inherited attributes are inlined (generated from schema)
; dschema: Datomic schema (generated from schema)

;;; schema

(defn read-schema []
  (Util/readAll
   (io/reader
    (io/resource "data/poc2/schema.edn"))))

(def schema (first (read-schema))) ; schema as is in the edn definition

;;; effective schema, where inherited attributes are inlined

(def get-eextends
  "Returns effective base types list for type"
  (memoize ; cache already computed results
   (fn [type]
     (let [extends  (-> schema type :extends)
           base-eextends (map get-eextends (-> schema type :extends))]
       (vec (flatten (cons base-eextends extends)))))))

(defn is-sub-type [t1 t2]
  "Returns true if t1 is sub type of t2"
  (lazy-contains? (get-eextends t1) t2))

(defn is-super-type [t1 t2]
  "Returns true if t1 is super type of t2"
  (lazy-contains? (get-eextends t2) t1))

(defn is-assignable-from [t1 t2]
  "Returns true if t1 = t2 or t1 is super type of t2"
  (or (= t1 t2) (is-super-type t1 t2)))

(defn get-overridden-fields [t f]
  (->> (filter  #(-> schema % :fields f) (get-eextends t))
       (map #(-> schema % :fields f))))

(defn is-overriding-fields [t f]
  (not (empty? (get-overridden-fields t f))))

(defn fix-attribute [a1 a2 ad a]
  "Fixes attribute a, returning attribute a' that correct a,
  or throws an exception if a is not compatible with the other ones"
  (if (lazy-contains? a2 a)
        (assert false "overriden fields attributes currently not supported"))
  (case a
    :cardinality-one
      (assert (not (lazy-contains? ad :cardinality-many)) ":cardinality-one is not compatible with :cardinality-many")
    :cardinality-many
      (assert (not (lazy-contains? ad :cardinality-one)) ":cardinality-many is not compatible with :cardinality-one")
    :identity
      (do
        (assert (not (lazy-contains? ad :cardinality-many)) ":identity is not compatible with :cardinality-many")
        (assert (not (lazy-contains? ad :track-valid-time)) ":identity is not compatible with :track-valid-time"))
    nil)
  a)

(defn merge-field-attributes [a1 a2]
  "Merges 2 field attributes sets.
  When attributes are different (but not in conflict) a2 attribute will take precedence.
  If a conflict is found in some attributes an exception will be thrown"
  (let [ad (distinct (concat a1 a2))]
    (if (or (not a1) (not a2))
      ad
      (remove nil? (map #(fix-attribute a1 a2 ad %) ad))
    )))

(defn merge-fields [k f1 f2]
  "Merges 2 field definitions.
  When type/attributes are different (but not in conflict) f2 type/attributes will take precedence.
  If a conflict is found an exception will be thrown"
  (if (or (not f1) (not f2))
    (concat f1 f2) ; if one of the two field definitions is nil, just return the other one
    (let [t1 (first f1)  ; type of field f1
          t2 (first f2)  ; type of field f2
          a1 (second f1) ; the attributes of field f1
          a2 (second f2) ; the attributes of field f2
          d1 (nth f1 2)  ; documentation for field f1
          d2 (nth f2 2)] ; documentation for field f2
      (assert (is-assignable-from t1 t2) (str "Cannot redifine field " k " with more generic type: " t1 " -> " t2))
      [t2 (merge-field-attributes a1 a2) d2])))

(def merge-field-maps
  "Returns a field map built by merging field maps t1f and t2f"
  (memoize ; cache already computed results
   (fn [t1f t2f]
    (let [field-keys (distinct (concat (keys t1f) (keys t2f)))] ; all fields keys, from both t1f and t2f
      (apply merge (map (fn [k] {k (merge-fields k (t1f k) (t2f k))}) field-keys))))))

(def get-efields
  "Returns the effective field definition set for type"
  (memoize ; cache already computed results
   (fn [t]
    (let [extends          (-> schema t :extends)
          extends-fields   (mapv get-efields extends)
          type-fields      (-> schema t :fields)
          all-types-fields (conj extends-fields type-fields)]
      (reduce merge-field-maps all-types-fields))))) ; merge all base effective schemas

(def eschema
  (mapcat (fn [[type typedef]]
                   [type (assoc typedef
                           :extends (get-eextends type)
                           :fields  (get-efields type))]) schema))

(defn write-eschema []
  (spit "generated/poc2/eschema.edn" (with-out-str (pprint eschema))))

(write-eschema)

;;; Datomic schema

(defn get-dtype [t]
  (case t
    :keyword :db.type/keyword
    :string  :db.type/string
    :int     :db.type/boolean
    :long    :db.type/long
    :bigint  :db.type/bigint
    :float   :db.type/float
    :double  :db.type/double
    :bigdec  :db.type/bigdec
    :instant :db.type/instant
    :uuid    :db.type/uuid
    :uri     :db.type/uri
    :bytes   :db.type/bytes
    (if (or (schema t) (= t :3e/class)) :db.type/ref nil)))

(defn generate-field-dschema [t [f fdef] include-schema-metadata]
  (let [flags (into #{} (second fdef))
        part :db.part/db
        field-dschema
          {:db/id (d/tempid part)
          :db/ident (keyword (str (name t) "/" (name f)))
          :db/valueType (get-dtype (first fdef))
          :db/cardinality (if (and (flags :cardinality-one) (not (flags :track-valid-time)))
                            :db.cardinality/one
                            :db.cardinality/many)
          :db/unique (if (flags :identity) :db.unique/identity
                       (if (and (flags :unique) (not (flags :track-valid-time))) :db.unique/value))
          :db/index (when (or (flags :index) (flags :identity)) true)
          :db/fulltext (when (flags :fulltext-search) true)
          :db/doc (nth fdef 2)
          :db.install/_attribute part}
        field-dschema-with-metedata
          (if include-schema-metadata
            (assoc field-dschema
              :3e.types/field-value-type (first fdef)
              :3e.validity/track (when (flags :track-valid-time) true)
              :3e.validity/cardinality (when (flags :track-valid-time)
                                       (if (flags :cardinality-one) :db.cardinality/one :db.cardinality/many)))
            field-dschema)
        filtered-dschema    (filter #(-> % val nil? not) field-dschema-with-metedata)] ; exclude attributes with nil values
          (into {} filtered-dschema)))

(defn generate-type-metainfo [t tdef]
  (let [tddef {:db/id (d/tempid :db.part/user)
               :db/ident t
               :db/doc (tdef :doc)
               :3e/type :3e/class}]
    (if (empty? (tdef :extends))
      tddef
      (assoc tddef :3e.types/extends (tdef :extends)))))

(defn generate-type-dschema [[t tdef] include-schema-metadata]
  (let [own-fields   (filter #(not (is-overriding-fields t (key %))) (tdef :fields)) ; ignore overridden fields in Datomic schema
        type-dschema (map #(generate-field-dschema t %1 include-schema-metadata) own-fields)]
    (if include-schema-metadata
      (concat [(generate-type-metainfo t tdef)] type-dschema)
      type-dschema)))

(def metadata-dschema
  (list (generate-field-dschema :3e          [:type [:3e/class [:cardinality-one] "The type of an entity"]] false)
        (generate-field-dschema :3e.validity [:from [:instant  [:cardinality-one]
                                          "Start validity time: to be included in each transaction that involves at least one attribute that has valid time tracking enabled"]] false)
        (generate-field-dschema :3e.validity [:to   [:instant  [:cardinality-one :index]
                                          "End validity time: to be included in each transaction that involves at least one attribute that has valid time tracking enabled. Defaults to infinity."]] false)))

(def metametadata-dschema
  (flatten
   (list
    (generate-type-dschema [:3e/class {:doc "Class type" :fields {}}] true)
    (generate-field-dschema :3e.validity [:track            [:boolean  [:cardinality-one]
                                                             (str "Valid time flag to be applied to attribute schema definitions "
                                                                  "to indicate that valid time should be tracked for the attribute")]] false)
    (generate-field-dschema :3e.validity [:cardinality      [:keyword  [:cardinality-one :index]
                                                             "Cardinality of valid time tracking fields"]] false)
    (generate-field-dschema :3e.types    [:extends          [:3e/class [:cardinality-many] "Extended types"]] false)
    (generate-field-dschema :3e.types    [:field-value-type [:3e/class [:cardinality-one] "Field value type"]] false))))

(defn generate-dschema [include-schema-metadata]
  (let [dschema (flatten (map #(generate-type-dschema % include-schema-metadata) schema))]
    (if include-schema-metadata
      (concat metametadata-dschema metadata-dschema dschema)
      (concat metadata-dschema dschema))))

(def dschema (generate-dschema false))
(def dschema-with-metadata (generate-dschema true))

(defn write-dschema []
  (spit "generated/poc2/dschema.edn" (with-out-str (pprint dschema))))

(defn write-dschema-with-metadata []
  (spit "generated/poc2/dschema-full.edn" (with-out-str (pprint dschema-with-metadata))))

(write-dschema)
(write-dschema-with-metadata)

;; datomic datastore connection

;; to start datomic transactor, cd to datomic folder, and:
; unix: bin/transactor config/samples/free-transactor-template.properties
; win:  bin\transactor config\samples\free-transactor-template.properties

;; to start the console, cd to datomic console folder, and:
; unix: bin/console -p 8090 dev datomic:dev://localhost:4334/
; win:  bin\console -p 8090 dev datomic:dev://localhost:4334/

(def uri "datomic:free://localhost:4334/poc2")

; database initialization

(defn read-dschema []
  (Util/readAll
   (io/reader
    (io/file "generated/poc2/dschema-full.edn"))))

; delete and recreate the database
(d/delete-database uri)
(d/create-database uri)

; establish a connection to the database
(def conn (d/connect uri))

(defn init-db []
  ; create database
  ; load schema
  (doseq [txd (read-dschema)]
    (d/transact conn [txd]))
  :done)

;; run init-db to initialize the database
(init-db)
