(defproject my-first-clojure-app "0.1.0-SNAPSHOT"
  :description "My first Clojure application"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/tools.logging "0.3.1"]
                 [ring/ring "1.2.1"]
                 [ring/ring-json "0.3.1"]
                 [compojure "1.2.1"]
                 [com.datomic/datomic-free "0.9.4699"]
                 [clj-time "0.8.0"]
                 [clojure-ini "0.0.2"]]
  :main ^:skip-aot dschema.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
